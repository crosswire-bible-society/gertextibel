\id 2PE
\toc2 2. Petrus
\toc1 Der zweite Brief des Petrus
\c 1
\p
\v 1 Symeon Petrus, Knecht und Apostel Jesus Christus an die, welche denselben kostbaren Glauben wie wir zugeteilt bekommen haben durch Gerechtigkeit unseres Gottes und Heilandes Jesus Christus.
\v 2 Gnade und Friede werde euch in Fülle durch die Erkenntnis Gottes und unseres Herrn Jesus.
\v 3 Wie uns seine göttliche Kraft alles, was zum Leben und zur Frömmigkeit gehört, geschenkt hat, mittelst der Erkenntnis dessen, der uns berufen hat, durch seine eigene Herrlichkeit und Tugend,
\v 4 wodurch uns die größten kostbaren Verheißungen geschenkt sind, damit ihr dadurch an der göttlichen Natur Anteil habet, entronnen dem Lustverderben in der Welt;
\v 5 so bringet nun eben darum mit Aufwendung alles Fleißes in eurem Glauben dar die Tugend, in der Tugend die Erkenntnis,
\v 6 in der Erkenntnis die Enthaltsamkeit, in der Enthaltsamkeit die Geduld, in der Geduld die Frömmigkeit,
\v 7 in der Frömmigkeit die Freundschaft, in der Freundschaft die Liebe.
\v 8 Denn wo diese Dinge bei euch vorhanden sind und wachsen, lassen sie euch nicht müßig noch ohne Frucht sein für die Erkenntnis unseres Herrn Jesus Christus.
\v 9 Denn bei wem sie nicht sind, der ist blind in Kurzsichtigkeit, und hat die Reinigung seiner früheren Sünden in Vergessenheit kommen lassen.
\v 10 Darum Brüder, wendet vielmehr Fleiß an, eure Berufung und Erwählung beständig zu machen; denn thut ihr dies, so werdet ihr nimmermehr fallen.
\v 11 Denn so wird euch reichlich gewährt werden der Eingang in das ewige Reich unseres Herrn und Heilandes Jesus Christus.
\v 12 Darum will ich euch stets an diese Dinge erinnern, wenn ihr sie auch schon wisset und in der vorhandenen Wahrheit fest gegründet seid.
\v 13 Ich achte es aber für Pflicht, so lange ich in dieser Hütte bin, euch durch Erinnerung wach zu halten,
\v 14 in dem Bewußtsein, daß es bald zur Ablegung meiner Hütte kommt, wie es mir auch unser Herr Jesus Christus kund gethan hat.
\v 15 Ich werde euch aber auch dafür sorgen, daß ihr jederzeit nach meinem Hingang im Stande seid, daran zu denken.
\v 16 Denn nicht, weil wir wohlausgesonnenen Fabeln folgten, haben wir euch die Macht und Gegenwart unseres Herrn Jesus Christus kund gethan, sondern weil wir Augenzeugen seiner Majestät waren.
\v 17 Nämlich wie er von Gott dem Vater Ehre und Ruhm empfangen hat, da von der hocherhabenen Herrlichkeit eine solche Stimme an ihn gelangte: dies ist mein geliebter Sohn, an welchem ich Wohlgefallen habe.
\v 18 Und diese Stimme haben wir vom Himmel kommen hören, da wir mit ihm waren auf dem heiligen Berge.
\v 19 Und so ist uns das prophetische Wort fest - woran ihr gut thut euch zu halten als an eine Leuchte, die da scheint an finsterem Ort, bis der Tag durchbricht und lichtbringend aufgeht in euren Herzen,
\v 20 darüber vor allem klar, daß keine Schriftweissagung eigene Lösung zuläßt,
\v 21 denn nie ist eine Weissagung durch menschlichen Willen geschehen, sondern getragen vom heiligen Geist haben von Gott aus Menschen geredet.
\c 2
\p
\v 1 Es traten aber auch falsche Propheten unter dem Volke auf, wie auch unter euch falsche Lehrer sein werden, die da werden Absonderungen zum Verderben einführen, indem sie sogar den Herrn, der sie erkauft hat, verleugnen, sich selbst jähes Verderben zuziehend.
\v 2 Und viele werden ihnen in ihren Ausschweifungen nachlaufen, um ihretwillen wird der Weg der Wahrheit gelästert werden,
\v 3 und sie werden an euch aus Habsucht mit trügerischen Worten Geschäfte machen; ihr Gericht aber ruht von alters her nicht, und ihr Verderben schlummert nicht.
\v 4 Hat doch Gott der Engel nicht geschont, die gesündigt hatten, sondern sie in die Hölle gestoßen, in die Gruben der Finsternis, zur Verwahrung auf das Gericht;
\v 5 und hat der alten Welt nicht geschont, sondern nur Noah den Herold der Gerechtigkeit selbacht geschützt, da er über die Welt der Gottlosen die Flut hereinführte;
\v 6 auch die Städte Sodom und Gomorrha hat er verurteilt, indem er sie in Asche legte, ein Vorbild des Kommenden gebend für die Gottlosen;
\v 7 doch den gerechten vom Wandel der Zuchtlosen in Ausschweifungen geplagten Lot hat er errettet;
\v 8 denn mit Sehen und Hören schöpfte der Gerechte, da er unter ihnen wohnte, Tag für Tag für seine gerechte Seele Qual durch frevelhafte Werke.
\v 9 Der Herr weiß Fromme aus Versuchung zu erretten, Ungerechte aber auf den Tag des Gerichtes zur Strafe zu bewahren,
\v 10 vornämlich die hinter dem Fleische her sind mit Begierde nach Befleckung, und Hoheit verachten: verwegen, frech, haben sie keine Scheu vor Herrlichkeiten: lästernd,
\v 11 wo doch Engel, die an Kraft und Macht größer sind, kein lästerndes Urteil gegen sie beim Herrn anbringen.
\v 12 Diese aber wie unvernünftige von Natur zu Fang und Verderben geborene Tiere, lästernd über das, wovon sie nichts wissen, werden in ihrem Verderben selbst verderben,
\v 13 zum Lohn empfangend ihre eigene Ungerechtigkeit: die da die Schlemmerei des Tages für Genuß achten, Schmutz- und Schandflecken, die bei ihren Liebesmahlen schwelgend mit euch zusammen tafeln,
\v 14 die Augen erfüllt von der Ehebrecherin und unersättlich in der Sünde, unbefestigte Seelen verlockend, das Herz ausgebildet in den Künsten der Habsucht, Kinder des Fluches.
\v 15 Den geraden Weg verlassend, sind sie irre gegangen, ausweichend auf den Weg des Balaam, des Sohnes des Bosor, der den Lohn der Ungerechtigkeit liebte,
\v 16 aber auch die Zurechtweisung seiner Gesetzesübertretung davon hatte: ein stummes Lasttier, in menschlicher Sprache redend, wehrte der Sinnesverkehrtheit des Propheten.
\v 17 Wasserlose Quellen sind sie, Nebel vom Sturmwinde getrieben, die Nacht der Finsternis ist für sie bereit gehalten.
\v 18 Denn indem sie überschwängliche Reden nichtigen Inhalts ertönen lassen, verlocken sie durch Fleisches Lüste und Schwelgereien die, welche kaum der Gesellschaft des Irrwegs entflohen sind,
\v 19 ihnen Freiheit versprechend, die sie selbst Sklaven des Verderbens sind. Denn von wem einer besiegt ist, dem ist er auch als Sklave verfallen.
\v 20 Denn wenn die, welche die Befleckungen der Welt durch die Erkenntnis des Herrn und Heilandes Jesus Christus meiden gelernt hatten, neuer Verstrickung darin erliegen, so ist bei ihnen das letzte schlimmer geworden, als das erste.
\v 21 Denn es wäre besser für sie, sie hätten den Weg der Gerechtigkeit nicht kennen gelernt, als daß sie ihn erkannt, und sich dann von dem ihnen mitgeteilten heiligen Gebote wieder abgewendet haben.
\v 22 Es ist bei ihnen eingetroffen, was das wahre Sprichwort sagt: der Hund, der sich zu seinem Auswurf wendet, und die Sau, die sich zum Kotwälzen badet.
\c 3
\p
\v 1 Dies ist schon der zweite Brief, Geliebte, welchen ich euch schreibe, um euch durch Erinnerung den reinen Sinn wachzuhalten,
\v 2 daß ihr gedenket der voraus von den heiligen Propheten gesprochenen Worte, und des von euren Aposteln mitgeteilten Gebotes des Herrn und Heilandes,
\v 3 darüber vor allem klar, daß am Ende der Tage mit Spott kommen werden die Spötter, die nach ihren eigenen Lüsten wandeln
\v 4 und sagen: wo ist die Verheißung seiner Ankunft? Seit der Zeit daß die Väter schlafen gegangen sind, bleibt ja alles so vom Anfang der Welt her.
\v 5 Denn sie merken nicht bei dieser Behauptung, daß vormals die Himmel und die Erde bestanden aus Wasser und mittelst Wassers durch das Wort Gottes,
\v 6 zufolge dessen die damalige Welt durch Wasserflut zu Grunde gieng,
\v 7 die jetztigen Himmel aber und die Erde durch das nämliche Wort aufgespart sind fürs Feuer, bewahrt auf den Tag des Gerichtes und Verderbens der gottlosen Menschen.
\v 8 Das eine aber möge euch nicht verborgen bleiben, Geliebte, daß ein Tag bei dem Herrn wie tausend Jahre und tausend Jahre wie ein Tag.
\v 9 Der Herr ist nicht langsam mit der Verheißung, wie es einige für Langsamkeit halten, sondern er ist langmütig für uns, und will nicht daß etliche verloren gehen, sondern daß alle zur Buße gelangen.
\v 10 Es wird aber der Tag des Herrn kommen, wie ein Dieb, wo die Himmel mit Krachen verschwinden, die Elemente im Brand sich auflösen, ebenso die Erde, und es wird sich zeigen, welche Werke auf ihr sind.
\v 11 Wenn nun dies alles sich auflöst, wie müssen sich in heiligem Wandel und Frömmigkeit halten,
\v 12 die da erwarten und ersehnen die Ankunft des Tages Gottes, um deswillen die Himmel im Feuer vergehen und die Elemente im Brande schmelzen werden,
\v 13 wir aber gemäß seiner Verheißung auf neue Himmel und eine neue Erde warten, in welchen Gerechtigkeit wohnt.
\v 14 Darum, Geliebte, indem ihr solches erwartet, trachtet unbefleckt und ohne Fehl bei ihm erfunden zu werden im Frieden;
\v 15 und achtet die Langmut des Herrn für Heil, wie auch unser geliebter Bruder Paulus nach der ihm verliehenen Weisheit an euch geschrieben hat,
\v 16 ebenso wie in allen seinen Briefen, da er von diesen Dingen redet, worin wohl einiges Schwerverständliche vorkommt, was die Ungelehrten und Unbefestigten verdrehen wie auch die übrigen Schriften, zu ihrem eigenen Verderben.
\v 17 Ihr nun, Geliebte, nehmet euch, da ihr es vorauswisset, in acht, daß ihr euch nicht durch den Irrtum der Zuchtlosen mit fortreißen lasset, und euren festen Halt verlieret.
\v 18 Wachset dagegen in der Gnade und Erkenntnis unseres Herrn und Heilandes Jesus Christus. Sein ist die Herrlichkeit jetzt und am Tage der Ewigkeit.
