\id MAL
\toc2 Maleachi
\toc1 Der Prophet Maleachi
\c 1
\p
\v 1 Ausspruch. Das Wort Jahwes an Israel durch Maleachi.
\s Jahwes Liebe zu Israel.
\p
\v 2 Ich habe Liebe zu euch, spricht Jahwe. Und fragt ihr: Worin zeigte sich deine Liebe zu uns? so lautet darauf der Spruch Jahwes: Esau ist doch ein Bruder Jakobs; aber ich liebte Jakob
\v 3 und Esau haßte ich, so daß ich seine Berge zur Einöde werden und sein Erbteil den Wüstenschakalen anheimfallen ließ.
\v 4 Wenn Edom etwa denkt: Zwar ist Zerstörung über unser Land gekommen, aber wir werden auch Trümmer wieder aufbauen können! so spricht Jahwe der Heerscharen also: Sie mögen bauen, ich aber werde niederreißen, so daß man ihnen den Namen geben wird: “Frevelgebiet” und “das Volk, dem Jahwe auf ewig grollt”.
\v 5 Mit eigenen Augen werdet ihr es sehen und werdet selbst sagen müssen: Groß ist Jahwe weit über den Bereich Israels hinaus!
\s Die Unehrbietigkeit der Priester.
\p
\v 6 Ein Sohn hat seinen Vater zu ehren und ein Sklave seinen Herrn. Nun, wenn ich Vater bin, wo bleibt denn die Ehre, die mir gebührt, und wenn ich Herr bin, wo bleibt denn die Ehrfurcht, die man mir schuldet? spricht Jahwe der Heerscharen zu euch, ihr Priester, die ihr meinen Namen verunehrt. Ihr fragt: Wiefern haben wir deinen Namen verunehrt?
\v 7 Ihr bringt ja unreine Opferspeise dar auf meinem Altare! Und ihr könnt noch fragen: Wiefern haben wir dich verunehrt? während ihr doch sprecht: Der Tisch Jahwes ist uns zu schlecht!
\v 8 Und wenn ihr ein blindes Tier als Opfer darbringt, so ist das nach eurer Meinung nichts Schlimmes, und wenn ihr ein lahmes oder krankes darbringt, so ist das auch nichts Schlimmes! Bringe es doch einmal deinem Statthalter zum Geschenk - ob er dir dann wohl günstig gesinnt sein oder dir Huld erweisen wird! spricht Jahwe der Heerscharen.
\v 9 Nun also, begütigt doch Gott, damit er uns Gnade erweise! Von eurer Hand ist solches geschehen; kann er da noch einem von euch Huld erweisen? spricht Jahwe der Heerscharen.
\v 10 Schlösse doch einer von euch lieber gleich die Tempelthüren zu, damit ihr nicht mehr vergeblich auf meinem Altare Feuer anfachtet! Es liegt mir nichts an euch, spricht Jahwe der Heerscharen, und Opfergaben aus eurer Hand begehre ich nicht.
\v 11 Denn vom Aufgang bis zum Untergang der Sonne ist unter den Nationen mein Name groß, und überall wird meinem Namen Rauchopfer und reine Opfergabe dargebracht; denn mein Name ist groß unter den Nationen, spricht Jahwe der Heerscharen.
\v 12 Ihr aber entweiht ihn, indem ihr denkt: Der Tisch Jahwes ist wertlos, und das, was für uns davon abfällt, zu essen, ist uns zuwider.
\v 13 Ihr sagt: Was kostet es doch für Mühe, es zu essen! und verschmäht es, spricht Jahwe der Heerscharen, ihr bringt Geraubtes herbei und was lahm und was krank ist, und bringt es als Opfer dar: sollte ich solches von eurer Hand begehren? spricht Jahwe.
\v 14 Vielmehr: Verflucht ein Betrüger, der, wenn er in seiner Herde ein männliches Tier hat und ein Gelübde that, dem Herrn dann ein schäbiges Muttertier opfert! Denn ein großer König bin ich, spricht Jahwe der Heerscharen, und gefürchtet ist mein Name unter den Nationen!
\c 2
\s Die Unehrbietigkeit der Priester. (Fortsetzung)
\p
\v 1 An euch ergeht demgemäß nun die folgende Anweisung, ihr Priester:
\v 2 Wenn ihr nicht hört und es euch nicht von Herzen angelegen sein laßt, meinem Namen Ehre zu geben, spricht Jahwe der Heerscharen, so entsende ich wider euch den Fluch und verfluche eure Gefälle; ja, ich habe sie bereits so gut wie verflucht, weil es euch gar nicht am Herzen liegt.
\v 3 Fürwahr, ich werde euch den Arm verwünschen und lähmen und euch Unrat ins Gesicht streuen, den Unrat eurer Feste, und euch zu ihm hinausschaffen!
\v 4 Dann werdet ihr zur Einsicht kommen, daß ich diese Anweisung an euch erlassen habe, damit mein Bund mit Levi bestehen bleibe, spricht Jahwe der Heerscharen.
\v 5 Der Bund, den ich mit ihm eingegangen war, verhießt ihm Leben und Glück, und ich gab ihm beides als Grund zur Ehrfurcht, und er fürchtete mich und war voll Scheu vor meinem Namen.
\v 6 Wahrhaftige Weisung war in seinem Munde, und kein Falsch war auf seinen Lippen zu finden; in Unsträflichkeit und Geradheit wandelte er nach meinem Willen und brachte viele ab von Ungerechtigkeit.
\v 7 Denn eines Priesters Lippen sollen sich an die rechte Lehre halten, und Unterweisung erwartet man aus seinem Munde; denn der Gesandte Jahwes der Heerscharen ist er.
\v 8 Ihr aber seid vom Wege abgewichen, habt mit eurer Unterweisung viele zu Fall gebracht; ihr habt den Bund mit Levi zerstört, spricht Jahwe der Heerscharen.
\v 9 Darum habe nun auch ich euch bei allem Volk in tiefe Verachtung gebracht, weil ihr euch ja doch nicht an meine Wege haltet und euch bei eurer Unterweisung parteiisch zeigt.
\s Gegen die Verheiratung mit Heidinnen und die Scheidung von Israelitinnen.
\p
\v 10 haben wir denn nicht alle denselben Vater? Hat nicht ein und derselbe Gott uns erschaffen? Warum handeln wir denn treulos gegeneinander, so daß wir den Bund unserer Väter entweihen?
\v 11 Treubruch hat Juda begangen, und Greuel sind in Israel und zu Jerusalem verübt worden; denn Juda hat, was Jahwe heilig war, entweiht, indem es Töchter eines fremden Gottes liebgewonnen und gefreit hat.
\v 12 Möge Jahwe jeden, der solches verübt, Kläger und Verteidiger in den Zelten Jakobs und solche, die Jahwe der Heerscharen Opfergaben darbringen, ausrotten!
\v 13 Zum Zweiten aber thut ihr Folgendes: Ihr bedeckt den Altar Jahwes mit Thränen, mit Weinen und Schluchzen, weil von einem freundlichen Blick auf die Opfer und von einer Entgegennahme wohlgefälliger Gaben aus eurer Hand nicht mehr die Rede sein kann.
\v 14 Ihr fragt noch: Warum? Darum, weil Jahwe Zeuge war bei dem Eingehen des Bundes zwischen dir und dem Weibe deiner Jugend, der du nun die Treue gebrochen hast, obschon sie deine Lebensgefährtin und deine durch feierliche Gelübde mit dir verbundene Gattin war.
\v 15 Hat nicht einer und derselbe uns das Leben gegeben und erhalten? Und was verlangt der eine? Kinder Gottes! So hütet euch wohl in eurem Sinn, und dem Weibe deiner Jugend werde nie die Treue gebrochen!
\v 16 Denn ich hasse Scheidung, spricht Jahwe, der Gott Israels, und den, der sein Gewand mit Frevel bedeckt, spricht Jahwe der Heerscharen; darum hütet euch wohl in eurem Sinn und brecht niemals die Treue!
\s Das nahe göttliche Gericht.
\p
\v 17 Ihr habt Jahwe viel geärgert mit euren Reden und ihr fragt noch: Wiefern haben wir ihn geärgert? Damit, daß ihr sprecht: Jeder, der übel thut, ist Jahwe wohlgefällig, und an solchen hat er seine Freude, oder wo ist denn sonst der Gott des Gerichts?
\c 3
\s Das nahe göttliche Gericht. (Fortsetzung)
\p
\v 1 Fürwahr, ich werde euch meinen Boten senden, daß er den Weg vor mir bahne; gar plötzlich wird der Herr, den ihr herbeiwünscht, in seinem Tempel eintreffen, und der Engel des Bundes, nach dem ihr begehrt, trifft alsbald ein, spricht Jahwe der Heerscharen.
\v 2 Wer aber kann es dann aushalten, wenn er kommt, und wer kann bestehen, wenn er erscheint? Denn er gleicht dem Feuer eines Schmelzers und der Lauge von Wäschern.
\v 3 Er wird sich hinsetzen, wie um Silber zu schmelzen und zu reinigen, und wird die Leviten reinigen und sie läutern wie Gold und wie Silber, damit Jahwe wieder solche habe, die in würdiger Weise Opfer darbringen,
\v 4 und damit die Opfer Judas und Jerusalems Jahwe wiederum angenehm seien, wie in den Tagen der Vorzeit in längstvergangenen Jahren.
\v 5 Ich werde an euch herantreten, um Gericht zu halten, und ein schneller Zeuge sein gegen die Zauberer, die Ehebrecher und die Meineidigen, gegen die, welche den Taglöhner, Witwen und Waisen übervorteilen und welche ohne Scheu vor mir Fremdlinge bedrücken, spricht Jahwe der Heerscharen.
\v 6 Denn ich, Jahwe, habe mich nicht geändert, und doch seid ihr noch die gleichen Jakobssöhne geblieben.
\v 7 Seit den Tagen eurer Väter seid ihr beständig von meinen Satzungen abgewichen und habt sie nicht beobachtet. Bekehrt euch zu mir, so will ich mich zu euch kehren, spricht Jahwe der Heerscharen. Aber ihr fragt noch: Worin sollen wir uns denn bekehren?
\v 8 Darf denn ein Mensch Gott betrügen, daß ihr mich betrügt? Ihr fragt: Wobei haben wir dich denn betrogen? Bei dem Zehnten und dem Hebeopfer!
\v 9 Mit dem Fluche seid ihr ja eben belegt, weil ihr mich betrügt, eure ganze Nation.
\v 10 Bringt den Zehnten ganz in das Schatzhaus, daß sich Zehrung in meinem Hause befinde, und versucht es einmal auf diese Weise mit mir, spricht Jahwe der Heerscharen, ob ich euch dann nicht die Fenster des Himmels öffne und euch bis zum Übermaß mit Segen überschütte!
\v 11 Dann verwünsche ich euretwegen die Freßheuschrecke, daß sie euch die Früchte des Bodens nicht mehr zerstören, und der Weinstock im Felde euch nicht mehr fehltragen soll, spricht Jahwe der Heerscharen.
\v 12 Da werden euch dann alle Nationen glücklich preisen, weil ihr ein Land des Entzückens sein werdet, spricht Jahwe der Heerscharen.
\v 13 Ihr nehmt euch in euren Reden viel gegen mich heraus, spricht Jahwe. Und ihr fragt noch: Was haben wir denn untereinander gegen dich geredet?
\v 14 Ihr spracht: Es ist nutzlos, Gott zu dienen, oder was hatten wir davon, daß wir uns an seine Ordnung hielten und daß wir um Jahwes der Heerscharen willen in schwarzer Trauer einhergingen?
\v 15 Darum preisen wir jetzt die Übermütigen glücklich; nicht nur kamen sie vorwärts, als sie Frevelthaten verübten, sondern sogar, als sie Gott versuchten, gingen sie straflos aus.
\v 16 Solches reden die, die Jahwe fürchten, zu einander, und Jahwe merkt auf und hört, und vor ihm wird für die, die Jahwe fürchten und vor seinem Namen Achtung haben, eine Gedenkschrift aufgezeichnet.
\v 17 Sie sollen mir, spricht Jahwe der Heerscharen, an dem Tage, da ich einschreite, zu eigen gehören, und ich werde Erbarmen mit ihnen haben, wie einer mit seinem Sohn Erbarmen hat, der ihn ehrt.
\v 18 Dann werdet ihr wieder den Unterschied sehen, der zwischen fromm und gottlos, zwischen dem, der Gott dient, und dem, der ihm nicht dient, besteht.
\v 19 Denn fürwahr, der Tag kommt, brennend heiß wie das Feuer im Ofen. Alle Übermütigen und alle, die Frevelthaten verübten, werden dann Stoppeln sein, und der Tag, der da kommt, spricht Jahwe der Heerscharen, wird sie versengen, so daß weder Wurzel noch Zweig von ihnen übrig bleibt.
\v 20 Euch aber, die ihr meinen Namen fürchtet, wird die Sonne der Gerechtigkeit aufgehen, Heilung unter ihren Fittigen bergend, und ihr werdet hervorkommen und hüpfen wie Kälber, die aus dem Stall herausgelassen werden,
\v 21 und werdet die Gottlosen zertreten, daß sie unter den Sohlen eurer Füße zu Asche werden, an dem Tage, den ich bereite, spricht Jahwe der Heerscharen.
\s Nachtrag: Die Sendung Elias.
\p
\v 22 Gedenkt an das Gesetz meines Knechtes Mose, dem ich am Horeb Satzungen und Rechte für ganz Israel anbefohlen habe.
\v 23 Fürwahr, ich werde euch den Propheten Elia senden, bevor der große und furchtbare Tag Jahwes hereinbricht,
\v 24 daß er die Väter mit den Söhnen und die Söhne mit den Vätern aussöhne, damit ich nicht komme und den Bannfluch an dem Lande vollstrecke.
